package com.bank.transfer.internal.service;

import com.bank.transfer.internal.event.TransactionEvent;
import com.bank.transfer.internal.model.ReservedLimitResponse;
import com.bank.transfer.internal.model.TransferRequest;
import com.bank.transfer.internal.model.UserLimitResponse;
import com.bank.transfer.internal.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Slf4j
@Service
public class InternalTransferService {
    @Value("${camel.lra.coordinator-url}")
    private String SAGA_URL;
    private static String SAGA_PATH = "/lra-coordinator/";
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${transaction.kafka.topic.transtaction-status}")
    private String TRANSACTION_STATUS_TOPIC;

    public String buildRequest(@Body TransferRequest request,
                               @Header("Long-Running-Action") String sagaId) {
        notifyTransactionEvent(request, sagaId, "INIT");
        return ObjectUtil.objectToString(request);
    }

    public String completeRequest(@Body String txnDetail,
                                  @Header("Long-Running-Action") String sagaId) {
        log.info("request completed: " + txnDetail);
        ReservedLimitResponse response =
                (ReservedLimitResponse) ObjectUtil.stringToObject(txnDetail, ReservedLimitResponse.class);
        TransferRequest request = response.getTransferRequest();
        notifyTransactionEvent(request, sagaId, "COMPLETED");
        return "Transaction " + request.getClientTransactionId() + " processed";
    }

    public void failRequest(@Header("Long-Running-Action") String sagaId) {
        log.info("request fail: " + sagaId);
        notifyTransactionEvent(null, sagaId, "FAIL");
    }

    public void notifyTransactionEvent(TransferRequest request, String sagaId, String status) {
        TransactionEvent event = new TransactionEvent();
        event.setSagaId(getSagaId(sagaId));
        event.setStatus(status);
        if (null != request) {
            event.setClientTransactionId(request.getClientTransactionId());
            event.setAmount(request.getAmount());
            event.setBeneficiary(request.getBeneficiary());
            event.setFromCif(request.getFromCif());
            event.setProductCode(request.getProductCode());
            event.setProductId(request.getProductId());
        }
        String eventStr = ObjectUtil.objectToString(event);
        log.info(" send transaction status to Topic : " + TRANSACTION_STATUS_TOPIC + " event: " + eventStr);
        kafkaTemplate.send(TRANSACTION_STATUS_TOPIC, eventStr);
    }

    private String getSagaId(String sagaPath) {
        return sagaPath.toUpperCase(Locale.ROOT).replace(SAGA_URL.toUpperCase(Locale.ROOT)
                + SAGA_PATH.toUpperCase(Locale.ROOT), "");
    }

    public String responseError() {
        return "Error when processing request";
    }
}
